import React from 'react'
import Member from './Member'
import MemberList from './MemberList'
import MemberList2_Pagination from './MemberList2_Pagination'
import MemberList3_Pagination from './MemberList3_Pagination'

export default props => 
  <div className='container'>
    <MemberList3_Pagination />
  </div>
  