import React, {Component} from 'react'
import Member from './Member'

const fetch = require("node-fetch")

class MemberList extends Component{
  state = {
    members: [],
    isLoading: false,
    administrators: []
  }

  
  makeAdmin = email => {
    const administrators = [...this.state.administrators, email]
    this.setState({administrators})
  } 
  
  removeAdmin = (email) => {
    const administrators = this.state.administrators.filter(
      adminEmail => adminEmail !== email //só coloca no array os emails diferentes do email do parametro
    )
    this.setState({administrators})
  }

  componentDidMount(){
    this.setState({isLoading: true})
    fetch('https://api.randomuser.me/?nat=GB&results=50')
      .then(response => response.json())
      .then(json => json.results) //na API está 'results: [{dados},{dados}, ...]'
      .then(members => this.setState({
        members,
        isLoading: false
      }))
  }

  render(){
    const {members, isLoading} = this.state

    return(
      <div>
        <h1>Society Member</h1>
  
        {(isLoading) ? <span>loading...</span> : <span>{members.length} members</span>}

        {(members.length) ? 
          members.map((data, i) => 
          <Member 
            key={i} 
            admin={this.state.administrators.some(
              adminEmail => adminEmail === data.email
            )}
            name={data.name.first + ' ' + data.name.last}
            email={data.email}
            thumbnail={data.picture.thumbnail}
            makeAdmin={this.makeAdmin}
            removeAdmin={this.removeAdmin}
          />
            //{console.log(i, ' : ' ,this.state.members[i].email, '\n')}
          
          ) : <span>Currently 0 Members</span>
          
        }
      </div>
    )
  }
}

export default MemberList