import React, {Component} from 'react'
import Member from './Member'
import Pagination from 'react-paginating'

const fetch = require("node-fetch")
const qtd = 10 //quantidade de resultados (é o total de usuários buscados)
const limit = 1
const pageCount = 5
const itemsPerPage = qtd / pageCount //quantidade de usuários mostrados por página
const pessoas = 0

class MemberList extends Component{
  state = {
    members: [],
    isLoading: false,
    administrators: [],
    currentPage: 1
  }

  handlePageChange = (page, e) => {
    this.setState({
      currentPage: page
    })
  }

  
  makeAdmin = email => {
    const administrators = [...this.state.administrators, email]
    this.setState({administrators})
  } 
  
  removeAdmin = (email) => {
    const administrators = this.state.administrators.filter(
      adminEmail => adminEmail !== email //só coloca no array os emails diferentes do email do parametro
    )
    this.setState({administrators})
  }

  componentDidMount(){
    this.setState({isLoading: true})
    fetch(`https://api.randomuser.me/?nat=GB&results=${qtd}`)
      .then(response => response.json())
      .then(json => json.results) //na API está '{results: [{dados},{dados}, ...]}'
      .then(members => this.setState({
        members,
        isLoading: false
      }))
      {console.log('DidMount Array pessoas: ',pessoas)}
  }

  showUsers = (data, i) => {
    return(
      <Member 
      key={i}
      admin={this.state.administrators.some(
        adminEmail => adminEmail === data[i].email
      )}
      name={data[i].name.first + ' ' + data[i].name.last}
      email={data[i].email}
      thumbnail={data[i].picture.thumbnail}
      makeAdmin={this.makeAdmin}
      removeAdmin={this.removeAdmin}
    />
    )
  }

  showUsers2(data, inicio, fim){
    let resp = []
    for(let i = inicio; i < fim; i++){
      console.log('i: ',i, ' - inicio: ', inicio, ' - fim: ',fim)
      resp.push(<Member 
        key={i}
        admin={this.state.administrators.some(
          adminEmail => adminEmail === data[i].email
        )}
        name={data[i].name.first + ' ' + data[i].name.last}
        email={data[i].email}
        thumbnail={data[i].picture.thumbnail}
        makeAdmin={this.makeAdmin}
        removeAdmin={this.removeAdmin}
      />)
      console.log('Resp: ',resp)
      console.log('Tamanho do resp: ', resp.length)
      return(<div>{resp}</div>)
    }
  }

  preencheArray = (data, itemsPerPage) => {
    pessoas = []
    for(let i = 0; i < itemsPerPage; i++){
      pessoas.push(data[i])
    }
  }

  render(){
    const {members, isLoading, currentPage} = this.state
    return(
      <div>
        <h1>Society Member</h1>

        {(isLoading) ? <span>loading...</span> : <span>{members.length} members</span>}

        {/*(members.length) ? 
          this.showUsers2(members, currentPage-1, ((currentPage-1)+limit)) : <span>Currently 0 Members</span>
        */}

        {/*
          (members.length) ? members.map((data, i) => 
            <Member 
              key={i}
              name={data.name.first + ' ' + data.name.last}
              email={data.email}
              thumbnail={data.picture.thumbnail}
            />
          ) : <span>Currently 0 Members</span>
        */}

        {
          (members.length) ? this.preencheArray(members, itemsPerPage) : <span>Currently 0 Members</span>
        }
        {
          pessoas.map((data, i) => {
            return(
              <Member 
              key={i}
              name={data.name.first + ' ' + data.name.last}
              email={data.email}
              thumbnail={data.picture.thumbnail}
              />
            )
            //console.log('*' ,data.name)
          })
        }
        
        {/*(pessoas.length) ? pessoas.map((data, i) => 
          <Member 
            key={i}
            name={data[i].name.first + ' ' + data[i].name.last}
            email={data[i].email}
            thumbnail={data[i].picture.thumbnail}
          />
          ) : <span>Currently 0 Members</span>*/
        }
      
        
  
        <Pagination
          total={members.length}
          limit={limit}
          pageCount={pageCount}
          currentPage={currentPage}
        >
          {({
            pages,
            currentPage,
            hasNextPage,
            hasPreviousPage,
            previousPage,
            nextPage,
            totalPages,
            getPageItemProps
          }) => (
            <div>
              {console.log('totalPages: ',totalPages, 'Total de items: ', members.length)}
              <button
                {...getPageItemProps({
                  pageValue: 1,
                  onPageChange: this.handlePageChange
                })}
              >
                Início
              </button>

              {hasPreviousPage && (
                <button
                  {...getPageItemProps({
                    pageValue: previousPage,
                    onPageChange: this.handlePageChange
                  })}
                >
                  {'<'}                  
                </button>
              )}

              {pages.map(page => {
                let activePage = null 
                if(currentPage === page){
                  activePage = {backgroundColor: '#fdce09'}
                }
                return(
                  <button
                    {...getPageItemProps({
                      pageValue: page,
                      key: page,
                      style: activePage,
                      onPageChange: this.handlePageChange
                    })}
                  >
                    {page}
                  </button>
                )
              })}

              {hasNextPage && (
                <button
                  {...getPageItemProps({
                    pageValue: nextPage,
                    onPageChange: this.handlePageChange
                  })}
                >
                  {'>'}
                </button>
              )}

              <button
                {...getPageItemProps({
                  pageValue: totalPages,
                  onPageChange: this.handlePageChange
                })}
              >
                Last
              </button>
              {console.log('currentPage: ', currentPage)/*debug*/}
            </div>
          )}
        </Pagination>
      </div>
    )
  }
}

export default MemberList