import React, {Component} from 'react'

import { IoIosBeer } from "react-icons/io";

class Member extends Component{

  componentWillMount(){
    this.style={
      backgroundColor: 'gray'
    }
  }  

  componentWillUpdate(nextProps){
    this.style={
      backgroundColor: (nextProps.admin) ? 'green' : 'purple'
    }
  }

  shouldComponentUpdate(nextProps){
    return this.props.admin !== nextProps.admin
  }

  render(){
    const {name, thumbnail, email, admin, makeAdmin, removeAdmin} = this.props
    return(
      <div style={this.style}>
        <h1>{name} {(admin ? <IoIosBeer/> : null)}</h1>
        {
          (admin) ? 
          <button className='btn btn-warning btn-sm' onClick={() => removeAdmin(email)}>Remove Admin</button> :
          <button className='btn btn-success btn-sm' onClick={() => makeAdmin(email)}>Make Admin</button>
        }
        
        <img src={thumbnail} alt='profile picture' />
        <p><a href={`mailto:${email}`}>{email}</a></p>
      </div>
    )
  }
}

export default Member