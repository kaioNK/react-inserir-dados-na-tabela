import React, {Component} from 'react'
import Member from './Member'

import '../css/members.css'

const fetch = require("node-fetch")
const qtd = 30

class MemberList3_Pagination extends Component{
  state = {
    members: [],
    isLoading: false,
    currentPage: 1,
    todosPerPage: 5,
    administrators: []
  }

  makeAdmin = email => {
    const administrators = [...this.state.administrators, email]
    this.setState({administrators})
  } 
  
  removeAdmin = (email) => {
    const administrators = this.state.administrators.filter(
      adminEmail => adminEmail !== email //só coloca no array os emails diferentes do email do parametro
    )
    this.setState({administrators})
  }

  handleClick = (e) => {
    this.setState({
      currentPage: Number(e.target.id),
    })
  }

  componentDidMount(){
    this.setState({isLoading: true})
    fetch(`https://api.randomuser.me/?nat=GB&results=${qtd}`)
      .then(response => response.json())
      .then(json => json.results)
      .then(members => this.setState({
        members,
        isLoading: false
      }))
  
  }

  render(){
    const {members, currentPage, todosPerPage, isLoading} = this.state
    console.log('members: ', members)
    const indexOfLastTodo = currentPage * todosPerPage
    const indexOfFirstTodo = indexOfLastTodo - todosPerPage
    const currentTodos = members.slice(indexOfFirstTodo, indexOfLastTodo)

    console.log('currentTodos: ',currentTodos)
    console.log('indexOfLastTodo: ', indexOfLastTodo)
    console.log('indexOfFirstTodo:', indexOfFirstTodo)
    console.log('currentPage: ', currentPage)
    /*const renderTodos = currentTodos.map((member, index) => {
      return <li key={index}>{member}</li>
    })*/

    const renderTodos = (members.length) ? currentTodos.map((data, i) => {
      return(
        <Member 
          key={i} 
          admin={this.state.administrators.some(
            adminEmail => adminEmail === data.email
          )}
          name={data.name.first + ' ' + data.name.last}
          email={data.email}
          thumbnail={data.picture.thumbnail}
          makeAdmin={this.makeAdmin}
          removeAdmin={this.removeAdmin}
        />
      )
    }) : <span>Currently 0 Members</span>

    console.log('renderTodos: ', renderTodos)

    const pageNumbers = []
    for(let i = 1; i <= Math.ceil(members.length / todosPerPage); i++){
      pageNumbers.push(i)
    }

    const renderPageNumbers = pageNumbers.map(number => {
      return(
        <li
          key={number}
          id={number}
          onClick={this.handleClick}
        >
          {number}
        </li>
      )
    })

    return(
      <div>
        <h1>Society Member</h1>
        {(isLoading) ? <span>loading...</span> : <span>{members.length} members</span>}

        <div>{renderTodos}</div>

        <ul id="page-numbers">
          {renderPageNumbers}
        </ul>
      </div>
    )
  }
}

export default MemberList3_Pagination