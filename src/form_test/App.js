import React, {Component} from 'react'
import {Form} from './Form'
import {Table} from './Table'

import 'bootstrap/dist/css/bootstrap.min.css'

class App extends Component{

  state = {
    myData: [
      {
        nome: 'Carlos',
        sexo: 'Masculino',
        email: 'carlos@teste.com',
        nascimento: '1991-01-26',
        cidade: 'São Paulo'
      }
    ]
  }

  info = (data) => {
    console.log('Dados: ',data)
    this.setState({
      myData: [...this.state.myData, data]
    })

    console.log(this.state.myData)
  }

  print = () => {
    console.log(this.state.myData)
  }

  render(){
    return(
      <div className="container">
        <Form teste={this.info} />

        <br/><br/>

        <Table data={this.state.myData} />

        <br/><br/>
        <button onClick={this.print}>Ver estado atual no console</button>
      </div>
    )
  }
}

export default App