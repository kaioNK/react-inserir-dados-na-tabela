import React from 'react'
import {TableRow} from './TableRow'

export const Table = ({data}) => {
  return(
    <table className='table'>
      <thead>
        <tr>
          <th>Nome</th>
          <th>Sexo</th>
          <th>Email</th>
          <th>Data de Nascimento</th>
          <th>Cidade</th>
        </tr>
      </thead>
      <tbody>
        {data.map((_data, i) => 
          <TableRow key={i} nome={_data.nome} sexo={_data.sexo} email={_data.email} nascimento={_data.nascimento} cidade={_data.cidade} />  
        )}
      </tbody>
    </table>
  )
}