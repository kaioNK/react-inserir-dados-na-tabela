import React from 'react'

export const TableRow = ({nome, sexo, email, nascimento, cidade}) => {
  return(
    <tr>
      <td>{nome}</td>
      <td>{sexo}</td>
      <td>{email}</td>
      <td>{nascimento}</td>
      <td>{cidade}</td>
    </tr>
  )
}