import React, {Component} from 'react'

import { TiArrowSortedDown } from "react-icons/ti";

const array_cidades = [
  'São Paulo',
  'Santos',
  'Rio de Janeiro',
  'Campinas',
  'Belo Horizonte',
  'Porto Alegre',
  'Curitiba'
]

class Autocomplete extends Component{

  get value(){
    return this.refs.inputCidades.value
  }
  
  set value(inputValue){
    this.refs.inputCidades.value = inputValue
  }

  render(){
    return(
      <div>
        <input type='text' list='list-cidades' ref='inputCidades' /> <TiArrowSortedDown/>
        <datalist id='list-cidades'>
          {this.props.options.map((opt, i) => 
            <option key={i}>{opt}</option>  
          )}
        </datalist>
      </div>
    )
  }
}

export const Form = ({teste}) => {

  //define as variaveis 
  let _name, _sexo, _email, _date, _city

  const submit = e => {
    e.preventDefault()
    
    //propriedade 'teste' como objeto
    teste({
      nome: _name.value,
      sexo: _sexo.value,
      email: _email.value,
      nascimento: _date.value,
      cidade: _city.value
    })

    //apaga os valores nos campos
    _name.value = ''
    _sexo.checked = false
    _email.value = ''
    _date.value = ''
    _city.value = ''
  }

  return(
    <form onSubmit={submit}>
      <div>
        <label>Nome:</label><br/>
        <input type='text' id='name' ref={input => _name = input} required />
      </div>
      <br/>
      <div>
        <label>Sexo:</label><br/>
        <input type='checkbox' ref={input => _sexo = input} value='Masculino' /><span>Masculino</span><br/>
        <input type='checkbox' id='date' ref={input => _sexo = input} value='Feminino' /> <span>Feminino</span>
      </div>
      <br/>
      <div>
        <label>Email:</label><br/>
        <input type='email' id='email' ref={input => _email = input} required />
      </div>
      <br/>
      <div>
        <label>Data Nascimento:</label><br/>
        <input type='date' id='date' ref={input => _date = input} required />
      </div>
      <br/>
      <div>
        <label>Cidade:</label>
        <Autocomplete options={array_cidades} ref={input => _city = input} />
      </div>
      <button>Send</button>
    </form>
  )
}